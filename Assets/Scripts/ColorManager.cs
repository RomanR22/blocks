﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ColorManager 
{

    public static Color GetRandomColor(int index, Color previous)
    {
        float r, g, b;

        if(index < 0)
            return new Color
            (
                r = Random.Range(0f, 1f),
                g = r + Random.Range(0.3f, 0.6f) * (Random.Range(0, 2) * 2 - 1),
                b = (r+g)/2f * (Random.Range(0, 2) * 2 - 1)
            );
        return new Color
        (
            previous.r + Random.Range(0.05f, 0.1f) * (Random.Range(0, 2) * 2 - 1),
            previous.g + Random.Range(0.05f, 0.1f) * (Random.Range(0, 2) * 2 - 1),
            previous.b + Random.Range(0.05f, 0.1f) * (Random.Range(0, 2) * 2 - 1)
        );
    }

}
