﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIManager : MonoBehaviour
{

    [SerializeField]
    private TextMeshProUGUI counterText;
    [SerializeField]
    private TextMeshProUGUI stateText;

    private int counterValue = 0;

    public int CounterValue
    {
        get {return counterValue;}
        private set {counterValue = value;}
    }

    private void OnEnable()
    {
        ResetCounter();
        BlocksSpawner.OnSpawnBlock += UpdateCounter;
        BlocksSpawner.OnChangeState += SetState;
    }

    public void UpdateCounter()
    {
        CounterValue++;
        counterText.text = counterValue.ToString(); ; 
    }

    public void ResetCounter()
    {
        CounterValue = 0;
        counterText.text = CounterValue.ToString();
    }

    public void SetState(GameState gameState)
    {
        switch (gameState)
        {
            case GameState.Start:
                stateText.text = "Press To Start";
                break;
            case GameState.Game:
                stateText.text = "";
                break;
            case GameState.GameOver:
                stateText.text = "Game Over";
                break;
        }
    }

    private void OnDisable()
    {
        BlocksSpawner.OnSpawnBlock -= UpdateCounter;
        BlocksSpawner.OnChangeState -= SetState;
    }

}
