﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MyGameManager : MonoBehaviour
{
    private BlocksSpawner blockSpawner;

    private GameState gameState;
    public GameState GameState
    {
        get { return gameState; }
        set { gameState = value; }
    }

    private void OnEnable()
    {
        BlocksSpawner.OnChangeState += SetState;
    }

    private void Awake()
    {
        blockSpawner = FindObjectOfType<BlocksSpawner>();
    }

    private void Start()
    {
        blockSpawner.SpawnNewBlock();
    }

    void Update()
	{
        switch(gameState)
        {
            case GameState.Start:
                if(Input.GetMouseButtonUp(0))
                    blockSpawner.ContinueGame();
                break;
            case GameState.Game:
                if(Input.GetMouseButtonDown(0))
                    blockSpawner.StopBlock();
                break;
            case GameState.GameOver:
                if(Input.GetMouseButtonDown(0))
                    SceneManager.LoadScene(0);
                break;
        }
	}

    private void SetState(GameState gameState)
    {
        GameState = gameState;

        switch(gameState)
        {
            case GameState.Start:
            case GameState.GameOver:
                Time.timeScale = 0f;
                break;
            case GameState.Game:
                Time.timeScale = 1f;
                break;
        }
    }

    private void OnDisable()
    {
        BlocksSpawner.OnChangeState -= SetState;
    }

}
