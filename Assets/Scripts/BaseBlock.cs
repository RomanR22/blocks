﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseBlock : MonoBehaviour, IBlock
{
    [SerializeField] private Renderer columnRenderer;

    private BlocksSpawner blocksSpawner;
    private Renderer blockRenderer;

    public Transform BlockTransform
    {
        get { return transform; }
        set { }
    }

    public Color BlockColor
    {
        get { return blockRenderer.material.color; }
        set { blockRenderer.material.color = value; }
    }

    public int BlockIndex { get; set; } = -1;

    private void Awake()
    {
        SetBlocksSpawner(FindObjectOfType<BlocksSpawner>());
        blockRenderer = GetComponent<Renderer>();
    }

    private void OnEnable()
    {
        blockRenderer.material.color = ColorManager.GetRandomColor(BlockIndex, Color.white);
        columnRenderer.material.color = blockRenderer.material.color;
    }

    public void SetBlocksSpawner(BlocksSpawner blocksSpawner)
    {
        this.blocksSpawner = blocksSpawner;
    }

    public void SpawnLeftover(Vector3 position, Vector3 size)
    {

    }
}
