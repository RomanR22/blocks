﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BlocksSpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject movingBlockPrefab;
    [SerializeField]
    private BaseBlock baseBlock;
    [SerializeField]
    private float blocksOffset = 1.5f;

    private List<MovingBlock> blocksList = new List<MovingBlock>();
    private Transform blocksSpawnerTransform;

    public static event Action OnSpawnBlock = delegate { };
    public static event Action<GameState> OnChangeState = delegate { };

    public float BlockOffset
    {
        get{return blocksOffset;}
        set{blocksOffset = value;}
    }

    public void Awake()
    {
        blocksSpawnerTransform = transform;
    }

    public void Start()
    {
        OnChangeState(GameState.Start);
    }

    public void ContinueGame()
    {
        OnChangeState(GameState.Game);
    }

    public void SpawnNewBlock()
    {
        var block = Instantiate(movingBlockPrefab, blocksSpawnerTransform);

        var movingBlock = block.GetComponent<MovingBlock>();
        movingBlock.SetBlocksSpawner(this);
        blocksList.Add(movingBlock);

        movingBlock.BlockIndex = blocksList.Count - 1;
        movingBlock.BlockColor = ColorManager.GetRandomColor(movingBlock.BlockIndex, GetPreviousBlock().BlockColor);
        movingBlock.BlockTransform.position = GetBlockStartPosition(movingBlock.BlockIndex);
        movingBlock.BlockTransform.localScale = movingBlock.BlockIndex != 0 ? GetPreviousBlock().BlockTransform.localScale : block.transform.localScale;
        movingBlock.BlockTransform.SetParent(blocksSpawnerTransform, true);
        movingBlock.MoveDirection = GetMoveDirection(movingBlock.BlockIndex);
        movingBlock.name = "Block_" + movingBlock.BlockIndex;
    }

    private Vector3 GetBlockStartPosition(int blockIndex)
    {
        float blockPositionX;
        float blockPositionY;
        float blockPositionZ;

        if(blocksList[blockIndex].BlockIndex == 0)
        {
            blockPositionX = 0f;
            blockPositionY = movingBlockPrefab.transform.position.y;
            blockPositionZ = blocksSpawnerTransform.position.z - blocksOffset;
        }
        else
        {
            blockPositionX = !BlocksResizer.IsEven(blocksList[blockIndex].BlockIndex) ? blocksSpawnerTransform.position.x - blocksOffset : GetPreviousBlock().BlockTransform.position.x;
            blockPositionY = GetPreviousBlock().BlockTransform.position.y + movingBlockPrefab.transform.localScale.y;
            blockPositionZ = BlocksResizer.IsEven(blocksList[blockIndex].BlockIndex) ? blocksSpawnerTransform.position.z - blocksOffset : GetPreviousBlock().BlockTransform.position.z;
        }

        return new Vector3
        (
            blockPositionX,
            blockPositionY,
            blockPositionZ
        );
    }

    private MoveDirection GetMoveDirection(int blockIndex)
    {
        return BlocksResizer.IsEven(blockIndex) ? MoveDirection.Z : MoveDirection.X;
    }

    public void StopBlock()
    {
        GetCurrentBlock().Stop();

        float overhang = BlocksResizer.GetOverhang(GetCurrentBlock(), GetPreviousBlock());
        float edge = BlocksResizer.GetEdge(GetCurrentBlock(), GetPreviousBlock());

        if (Mathf.Abs(overhang) >= edge)
        {
            OnChangeState(GameState.GameOver);
        }
        else
        {
            float moveDirection = overhang > 0 ? 1f : -1f;
            CutCube(overhang, moveDirection);
            OnSpawnBlock();
        }
    }

    private void CutCube(float overhang, float moveDirection)
    {
        float newSizeMovingBlock = BlocksResizer.GetLocalScaleMovingBlock(GetCurrentBlock(), GetPreviousBlock()) - Mathf.Abs(overhang);
        float newPositionMovingBlock = BlocksResizer.GetPositionMovingBlock(GetCurrentBlock(), GetPreviousBlock()) + (overhang / 2f);

        float sizeLeftoverBlock = BlocksResizer.GetSizeLeftover(GetCurrentBlock()) - newSizeMovingBlock;

        UpdateCurrentBlock(newSizeMovingBlock, newPositionMovingBlock, GetCurrentBlock());

        float leftoverEdge = BlocksResizer.GetLeftoverEdge(GetCurrentBlock()) + (newSizeMovingBlock / 2f * moveDirection);
        float positionLeftoverBlock = leftoverEdge + sizeLeftoverBlock / 2f * moveDirection;

        SpawnLeftover(sizeLeftoverBlock, positionLeftoverBlock, GetCurrentBlock(), GetPreviousBlock());

        SpawnNewBlock();
    }

    private void UpdateCurrentBlock(float sizeMovingBlock, float positionMovingBlock, IBlock currentBlock)
    {
        currentBlock.BlockTransform.localScale = BlocksResizer.GetRescaledMovingBlock(sizeMovingBlock, currentBlock);
        currentBlock.BlockTransform.position = BlocksResizer.GetRepositionedMovingBlock(positionMovingBlock, currentBlock);
    }

    private void SpawnLeftover(float sizeLeftoverBlock, float positionLeftoverBlock, IBlock currentBlock, IBlock previousBlock)
    {
        Vector3 size = BlocksResizer.GetRescaledLeftover(sizeLeftoverBlock, currentBlock, previousBlock);
        Vector3 position = BlocksResizer.GetRepositionedLeftover(positionLeftoverBlock, currentBlock);

        currentBlock.SpawnLeftover(position, size);
    }

    public MovingBlock GetCurrentBlock()
    {
        return blocksList.Last();
    }

    public IBlock GetPreviousBlock()
    {
        return GetCurrentBlock().BlockIndex == 0 ? baseBlock : blocksList[GetCurrentBlock().BlockIndex - 1].GetComponent<IBlock>();
    }

    public IBlock GetBlockByIndex(int index)
    {
        return blocksList[index];
    }

    public IBlock GetBaseBlock()
    {
        return baseBlock;
    }

}