﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingBlock : MonoBehaviour, IBlock
{
    [SerializeField]
    private float moveSpeed = 1f;
    [SerializeField]
    private float destroyLeftoverTime = 1f;

    private Renderer blockRenderer;
    private BlocksSpawner blocksSpawner;
    private int blockIndex = -1;
    private bool isMovingForward = true;
    private IEnumerator destroyLeftover;

    public int BlockIndex
    {
        get{return blockIndex;}
        set{blockIndex = value;}
    }
    public MoveDirection MoveDirection { get; set; }

    public Transform BlockTransform
    {
        get { return transform; }
        set { }
    }

    public Color BlockColor
    {
        get { return blockRenderer.material.color; }
        set { blockRenderer.material.color = value;}
    }

    private void Awake()
    {
        blockRenderer = GetComponent<Renderer>();
    }

    private void Update ()
	{
	    if(moveSpeed > 0f)
	    {
	        if(MoveDirection == MoveDirection.X)
	        {
	            if(isMovingForward)
	            {
	                BlockTransform.position += BlockTransform.right * Time.deltaTime * moveSpeed;
	                if(BlockTransform.position.x > blocksSpawner.BlockOffset) isMovingForward = false;
	            }
	            else
	            {
	                BlockTransform.position -= BlockTransform.right * Time.deltaTime * moveSpeed;
	                if(BlockTransform.position.x < -blocksSpawner.BlockOffset) isMovingForward = true;
	            }
	        }
	        else if(MoveDirection == MoveDirection.Z)
	        {
	            if(isMovingForward)
	            {
	                BlockTransform.position += BlockTransform.forward * Time.deltaTime * moveSpeed;
	                if(BlockTransform.position.z > blocksSpawner.BlockOffset) isMovingForward = false;
	            }
	            else
	            {
	                BlockTransform.position -= BlockTransform.forward * Time.deltaTime * moveSpeed;
	                if(BlockTransform.position.z < -blocksSpawner.BlockOffset) isMovingForward = true;
	            }
	        }
        }
	}

    public void SetBlocksSpawner(BlocksSpawner blocksSpawner)
    {
        this.blocksSpawner = blocksSpawner;
    }

    public void Stop()
    {
        moveSpeed = 0f;
    }

    public void SpawnLeftover(Vector3 position, Vector3 size)
    {
        var leftover = GameObject.CreatePrimitive(PrimitiveType.Cube);

        leftover.transform.position = position;
        leftover.transform.localScale = size;
        leftover.transform.SetParent(BlockTransform, true);

        leftover.GetComponent<Renderer>().material.color = blockRenderer.material.color;

        if(destroyLeftover != null) StopCoroutine(destroyLeftover);

        destroyLeftover = DestroyLeftover(leftover);
        StartCoroutine(destroyLeftover);
    }

    private IEnumerator DestroyLeftover(GameObject leftover)
    {
        Transform leftoverTransform = leftover.transform;

        Vector3 endPosition = new Vector3(leftoverTransform.position.x, leftoverTransform.position.y + 5f, leftoverTransform.position.z);
        Vector3 velocity = Vector3.zero;

        float distance = Vector3.Distance(leftoverTransform.position, endPosition);

        while(distance > Vector3.kEpsilon)
        {
            leftoverTransform.position = Vector3.SmoothDamp(leftoverTransform.position, endPosition, ref velocity, Time.deltaTime, 5f);
            distance = Vector3.Distance(leftoverTransform.position, endPosition);
            yield return null;
        }

        Destroy(leftover);
    }

}
