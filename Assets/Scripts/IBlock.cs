﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBlock
{

    Color BlockColor
    {
        get;
        set;
    }
    Transform BlockTransform
    {
        get;
        set;
    }
    int BlockIndex
    {
        get;
        set;
    }

    void SetBlocksSpawner(BlocksSpawner blocksSpawner);
    void SpawnLeftover(Vector3 position, Vector3 size);
}

public class BlocksPair
{
    BlocksPair blocksPair;

    private IBlock blockPrevious;
    private IBlock blockCurrent;

    public BlocksPair(IBlock blockPrevious, IBlock blockCurrent)
    {
        this.blockPrevious = blockPrevious;
        this.blockCurrent = blockCurrent;
    }

    public IBlock GetPrevious()
    {
        return blockPrevious;
    }

    public IBlock GetCurrent()
    {
        return blockCurrent;
    }

}