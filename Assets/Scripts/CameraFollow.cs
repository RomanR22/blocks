﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    private IEnumerator moveCamera;

    private void OnEnable()
    {
       BlocksSpawner.OnSpawnBlock += MoveCameraUp;
    }

    private void MoveCameraUp()
    {
        if(moveCamera != null) StopCoroutine(moveCamera);

        moveCamera = MoveCameraUpCoroutine();
        StartCoroutine(moveCamera);
    }

    private IEnumerator MoveCameraUpCoroutine()
    {
        Vector3 endPosition = new Vector3(transform.position.x, transform.position.y + 0.15f, transform.position.z);

        float distance = Vector3.Distance(transform.position, endPosition);

        while(distance > Vector3.kEpsilon)
        {
            distance = Vector3.Distance(transform.position, endPosition);
            transform.position = Vector3.MoveTowards(transform.position, endPosition, Time.deltaTime);
            yield return null;
        }

    }

    private void OnDisable()
    {
        BlocksSpawner.OnSpawnBlock -= MoveCameraUp;
    }

}
