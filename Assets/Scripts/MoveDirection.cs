﻿public enum MoveDirection
{
    X,
    Z
}

public enum Axis
{
    X,
    Y,
    Z
}