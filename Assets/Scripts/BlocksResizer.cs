﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BlocksResizer 
{
    public static bool IsEven(int number)
    {
        return number % 2 == 0;
    }

    public static float GetOverhang(IBlock blockCurrent, IBlock blockPrevious)
    {
        return IsEven(blockCurrent.BlockIndex) ? blockCurrent.BlockTransform.position.z - blockPrevious.BlockTransform.position.z :
            blockCurrent.BlockTransform.position.x - blockPrevious.BlockTransform.position.x;
    }

    public static float GetEdge(IBlock blockCurrent, IBlock blockPrevious)
    {
        return IsEven(blockCurrent.BlockIndex) ? blockPrevious.BlockTransform.localScale.z : blockPrevious.BlockTransform.localScale.x;
    }

    public static float GetLocalScaleMovingBlock(IBlock blockCurrent, IBlock blockPrevious)
    {
        return IsEven(blockCurrent.BlockIndex) ? blockPrevious.BlockTransform.localScale.z : blockPrevious.BlockTransform.localScale.x;
    }

    public static float GetPositionMovingBlock(IBlock blockCurrent, IBlock blockPrevious)
    {
        return IsEven(blockCurrent.BlockIndex) ? blockPrevious.BlockTransform.position.z : blockPrevious.BlockTransform.position.x;
    }

    public static Vector3 GetRescaledMovingBlock(float sizeMovingBlock, IBlock blockCurrent)
    {
        return new Vector3
        (
            !IsEven(blockCurrent.BlockIndex) ? sizeMovingBlock : blockCurrent.BlockTransform.localScale.x,
            blockCurrent.BlockTransform.localScale.y,
            IsEven(blockCurrent.BlockIndex) ? sizeMovingBlock : blockCurrent.BlockTransform.localScale.z
        );
    }

    public static Vector3 GetRepositionedMovingBlock(float positionMovingBlock, IBlock blockCurrent)
    {
        return new Vector3
        (
            !IsEven(blockCurrent.BlockIndex) ? positionMovingBlock : blockCurrent.BlockTransform.position.x,
            blockCurrent.BlockTransform.position.y,
            IsEven(blockCurrent.BlockIndex) ? positionMovingBlock : blockCurrent.BlockTransform.position.z
        );
    }

    public static float GetLeftoverEdge(IBlock blockCurrent)
    {
        return IsEven(blockCurrent.BlockIndex) ? blockCurrent.BlockTransform.position.z : blockCurrent.BlockTransform.position.x;
    }

    public static float GetSizeLeftover(IBlock blockCurrent)
    {
        return IsEven(blockCurrent.BlockIndex) ? blockCurrent.BlockTransform.localScale.z : blockCurrent.BlockTransform.localScale.x;
    }

    public static Vector3 GetRepositionedLeftover(float positionLeftoverBlock, IBlock blockCurrent)
    {
        return new Vector3
        (
            !IsEven(blockCurrent.BlockIndex) ? positionLeftoverBlock : blockCurrent.BlockTransform.position.x,
            blockCurrent.BlockTransform.position.y,
            IsEven(blockCurrent.BlockIndex) ? positionLeftoverBlock : blockCurrent.BlockTransform.position.z
        );
    }

    public static Vector3 GetRescaledLeftover(float sizeLeftoverBlock, IBlock blockCurrent, IBlock blockPrevious)
    {
        return new Vector3
        (
            !IsEven(blockCurrent.BlockIndex) ? sizeLeftoverBlock : blockPrevious.BlockTransform.localScale.x,
            blockCurrent.BlockTransform.localScale.y,
            IsEven(blockCurrent.BlockIndex) ? sizeLeftoverBlock : blockPrevious.BlockTransform.localScale.z
        );
    }
}
